import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import {LicenseManager} from 'ag-grid-enterprise';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

LicenseManager.setLicenseKey(`CompanyName=Medline Industries Inc.,LicensedGroup=Medline IS Department,
LicenseType=MultipleApplications,LicensedConcurrentDeveloperCount=4,LicensedProductionInstancesCount=0,
AssetReference=AG-013889,ExpiryDate=26_March_2022_[v2]_MTY0ODI1MjgwMDAwMA==f20b0d611b49588a768e01a7a1f98b3f`);

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.error(err));
