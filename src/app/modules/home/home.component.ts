import {Component, OnInit} from '@angular/core';
import {HomeService} from "../../shared/services/home.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public defaultColDef = {};
  public gridApi:any;
  public gridColumnApi:any;
  public rowData = [];
  public editorColumnDefs:any = [];

  public gridOptions = {
    rowStyle: {
      justifyContent: 'center',
      alignItems: 'center',
      border: '1px solid #eeeeee',
      width: '100%',
      background: 'none'
    },
    rowHeight: 40,
    headerHeight: 55,
    suppressRowClickSelection: true,
  };

  constructor(private homeService: HomeService) { }

  ngOnInit(): void {
    this.editorColumnDefs = this.homeService.getGridColumnDefs();

    this.defaultColDef = {
      width: 150,
      editable: true
    };
  }

  onGridReady(params:any) {
    this.gridApi = params.api; // To access the grids API
    this.gridColumnApi = params.columnApi;
  }

}
