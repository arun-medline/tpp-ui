import { Component, OnInit } from '@angular/core';
import {NgbModal } from "@ng-bootstrap/ng-bootstrap";
import {EmployeeComponent} from "../../shared/Components/employee/employee.component";

@Component({
  selector: 'app-ninebox',
  templateUrl: './ninebox.component.html',
  styleUrls: ['./ninebox.component.scss']
})
export class NineboxComponent implements OnInit {

  activeFilterBy:any = {};

  filterByValues = [
    {value: 0, name: '-All Manager Level-'},
    {value: 1, name: '23 - Advanced (P4/S4) '},
    {value: 2, name: '60 - Director (M4)'},
    {value: 3, name: '-All Manager Level-'},
    {value: 4, name: '-All Manager Level-'},
    {value: 5, name: '-All Manager Level-'},
    {value: 6, name: '-All Manager Level-'},
    {value: 7, name: '-All Manager Level-'},
    {value: 8, name: '-All Manager Level-'},
  ]
  constructor(private modalService: NgbModal) { }

  ngOnInit(): void {

    this.filterBy(0)
  }

  filterBy(value: number) {

    this.activeFilterBy = this.filterByValues.find((f) => f.value === value);
  }

  showEmployeeDetails() {
    const modalRef = this.modalService.open(EmployeeComponent, {size: 'lg'});
    modalRef.componentInstance.name = 'World';
  }

}
