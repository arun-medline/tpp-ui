import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NineboxComponent } from './ninebox.component';

describe('NineboxComponent', () => {
  let component: NineboxComponent;
  let fixture: ComponentFixture<NineboxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NineboxComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NineboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
