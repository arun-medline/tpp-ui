import {Component, OnInit} from '@angular/core';
import {OidcSecurityService} from "angular-auth-oidc-client";
import {faUser} from "@fortawesome/free-solid-svg-icons";
import {ActivatedRoute, Router,  NavigationEnd} from "@angular/router";
import {Subscription} from "rxjs";

enum MenuOptions {
  AuthError,
  home,
  ninebox,
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements  OnInit{

  menuOptions = MenuOptions;
  activeMenu = MenuOptions.home;
  subscription: Subscription;

  title = 'Talent Planning Process';
  userIcon = faUser;
  isAuthenticated = false;
  username = 'unknown, unknown'
  userData:any;



  constructor(public oidcSecurityService: OidcSecurityService, public router: Router, public route: ActivatedRoute) {

    this.subscription = router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        switch (event.url) {
          case '/':
            this.activeMenu = MenuOptions.home;
            break;
          case '/home':
            this.activeMenu = MenuOptions.home;
            break;
          case '/ninebox':
            this.activeMenu = MenuOptions.ninebox;
            break;
          default:
            this.activeMenu = MenuOptions.home;
            break;
        }
      }
    });
  }

  ngOnInit() {
    this.oidcSecurityService.isAuthenticated$.subscribe(({ isAuthenticated }) => {
      this.isAuthenticated = isAuthenticated;

      console.info('authenticated: ', isAuthenticated);
    });
    this.oidcSecurityService.checkAuth().subscribe(({ isAuthenticated, userData, accessToken, errorMessage }) => {
          this.userData = userData;
          this.username = userData.name;
    });

  }
}

