import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  constructor() { }

  getGridColumnDefs() {

    return [
        {
        headerName: 'Name',
        field: 'row',
        cellClass: [],
        editable: false,
        suppressClipboardPaste: true,
        suppressMenu: true
      },
      {
        headerName: 'Job Title',
        field: 'row',
        cellClass: [],
        editable: false,
        suppressClipboardPaste: true,
        suppressMenu: true
      },
      {
        headerName: 'Manager Level',
        field: 'row',
        cellClass: [],
        editable: false,
        suppressClipboardPaste: true,
        suppressMenu: true
      },
      {
        headerName: 'Job Code',
        field: 'row',
        cellClass: [],
        editable: false,
        suppressClipboardPaste: true,
        suppressMenu: true
      },
      {
        headerName: 'Hire Date',
        field: 'row',
        cellClass: [],
        editable: false,
        suppressClipboardPaste: true,
        suppressMenu: true
      },
      {
        headerName: 'Performance',
        field: 'row',
        cellClass: [],
        editable: false,
        suppressClipboardPaste: true,
        suppressMenu: true
      },
      {
        headerName: 'Potential',
        field: 'row',
        cellClass: [],
        editable: false,
        suppressClipboardPaste: true,
        suppressMenu: true
      },
      {
        headerName: 'Risk Of Loss',
        field: 'row',
        cellClass: [],
        editable: false,
        suppressClipboardPaste: true,
        suppressMenu: true
      },
      {
        headerName: 'Impact Of Loss',
        field: 'row',
        cellClass: [],
        editable: false,
        suppressClipboardPaste: true,
        suppressMenu: true
      },
      {
        headerName: '',
        field: 'row',
        cellClass: [],
        editable: false,
        suppressClipboardPaste: true,
        suppressMenu: true
      },
    ]

  }
}
