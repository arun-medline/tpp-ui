import { NgModule } from '@angular/core';
import { AuthModule } from 'angular-auth-oidc-client';


@NgModule({
    imports: [AuthModule.forRoot({
        config: {
          authority: 'https://ssodev.medline.com',
          redirectUrl: window.location.origin + '/callback',
          postLogoutRedirectUri: window.location.origin,
          clientId: 'cfw8aZjbDpMu1SeA7BGhYREKGYUa',
          scope: 'openid', // 'openid profile offline_access ' + your scopes
          responseType: 'code',
          silentRenew: true,
          useRefreshToken: true,
          renewTimeBeforeTokenExpiresInSeconds: 30,
          }
      })],
    exports: [AuthModule],
})
export class AuthConfigModule {}
